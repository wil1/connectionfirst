version: "3.7"

secrets:
  # secret name is in format ${STACK_NAME}_admin_password and must be renamed
  connectionfirst_admin_password:
    external: true
    
services:
  code:
    build:
      context: ../../code-server
    image: ${REGISTRY}/${USER}/${STACK_NAME}/code-server:${TAG}
    restart: always
    privileged: true
    ipc: host
    environment:
      STACK_NAME: ${STACK_NAME}
      TAG: ${TAG}
      BASE_DOMAIN: ${BASE_DOMAIN}
      EMAIL_DOMAIN: ${EMAIL_DOMAIN}
      VIRTUAL_HOST: code.${BASE_DOMAIN}
      VIRTUAL_PORT: 8080
      LETSENCRYPT_HOST: code.${BASE_DOMAIN}
      LETSENCRYPT_TEST: ${LETSENCRYPT_TEST}
      PASSWORD_FILE: /run/secrets/${STACK_NAME}_admin_password
    security_opt:
      - seccomp:unconfined
    volumes:
      - code_server:/home/code/.local/share/code_server
      - coder_home:/home/coder
    networks:
      - proxy_network
      - backend
    secrets:
      - ${STACK_NAME}_admin_password
  pgadmin:
    image: dpage/pgadmin4
    restart: always
    environment:
      VIRTUAL_HOST: pgadmin.${BASE_DOMAIN}
      VIRTUAL_PORT: 80
      LETSENCRYPT_HOST: pgadmin.${BASE_DOMAIN}
      LETSENCRYPT_TEST: ${LETSENCRYPT_TEST}
      PGADMIN_DEFAULT_EMAIL: admin@${BASE_DOMAIN}
      PASSWORD_FILE: /run/secrets/${STACK_NAME}_admin_password
    volumes:
      - ../../pgadmin/entrypoint-wrapper.sh:/entrypoint-wrapper.sh
    entrypoint: /entrypoint-wrapper.sh
    networks:
      - proxy_network
      - backend
    secrets:
      - ${STACK_NAME}_admin_password

  website:
    build:
      context: ../../socat-proxy
    image: ${REGISTRY}/${USER}/${STACK_NAME}/socat-proxy:${TAG}
    environment:
      VIRTUAL_HOST: ${BASE_DOMAIN}
      VIRTUAL_PORT: 8000
      SOCAT_PORT: 8000
      SOCAT_TARGET: code
      LETSENCRYPT_HOST: ${BASE_DOMAIN}
      LETSENCRYPT_TEST: ${LETSENCRYPT_TEST}
    networks:
      - proxy_network
      - backend

  adminsite:
    build:
      context: ../../socat-proxy
    image: ${REGISTRY}/${USER}/${STACK_NAME}/socat-proxy:${TAG}
    environment:
      VIRTUAL_HOST: admin.${BASE_DOMAIN}
      VIRTUAL_PORT: 8300
      SOCAT_PORT: 8300
      SOCAT_TARGET: code
      LETSENCRYPT_HOST: admin.${BASE_DOMAIN}
      LETSENCRYPT_TEST: ${LETSENCRYPT_TEST}
    networks:
      - proxy_network
      - backend
      

  api:
    build:
      context: ../../socat-proxy
    image: ${REGISTRY}/${USER}/${STACK_NAME}/socat-proxy:${TAG}
    environment:
      VIRTUAL_HOST: api.${BASE_DOMAIN}
      VIRTUAL_PORT: 3000
      SOCAT_PORT: 3000
      SOCAT_TARGET: code
      LETSENCRYPT_HOST: api.${BASE_DOMAIN}
      LETSENCRYPT_TEST: ${LETSENCRYPT_TEST}
      STACK_NAME: ${STACK_NAME}
      TAG: ${TAG}
      BASE_DOMAIN: ${BASE_DOMAIN}
    networks:
      - proxy_network
      - backend

  keycloak-postgres:
    image: postgres
    volumes:
      - keycloak_postgres_data:/var/lib/postgresql/data
    environment:
      POSTGRES_DB: keycloak
      POSTGRES_USER: keycloak
      POSTGRES_PASSWORD_FILE: /run/secrets/${STACK_NAME}_admin_password
    networks:
      - backend
    secrets:
      - ${STACK_NAME}_admin_password

  postgres:
    build:
      context: ../../postgres
    image: ${REGISTRY}/${USER}/${STACK_NAME}/postgres:${TAG}
    volumes:
      - postgres_data:/var/lib/postgresql/data
    environment:
      POSTGRES_USER: ${STACK_NAME}
      POSTGRES_DB:  ${STACK_NAME}
      POSTGRES_PASSWORD_FILE: /run/secrets/${STACK_NAME}_admin_password
    networks:
      - backend
    secrets:
      - ${STACK_NAME}_admin_password

  keycloak:
    image: jboss/keycloak
    environment:
      DB_VENDOR: POSTGRES
      DB_ADDR: keycloak-postgres
      DB_DATABASE: keycloak
      DB_USER: keycloak
      DB_SCHEMA: public
      DB_PASSWORD_FILE: /run/secrets/${STACK_NAME}_admin_password
      KEYCLOAK_USER: admin
      KEYCLOAK_PASSWORD_FILE: /run/secrets/${STACK_NAME}_admin_password
      PROXY_ADDRESS_FORWARDING: "true"
      VIRTUAL_HOST: keycloak.${BASE_DOMAIN}
      VIRTUAL_PORT: 8080
      LETSENCRYPT_HOST: keycloak.${BASE_DOMAIN}
      LETSENCRYPT_TEST: ${LETSENCRYPT_TEST}
    depends_on:
      - keycloak-postgres
    networks:
      - proxy_network
      - backend
    secrets:
      - ${STACK_NAME}_admin_password

volumes:
  code_server:
  coder_home:
  keycloak_postgres_data:
  postgres_data:

networks:
  backend:
  proxy_network:
    external:
      name: nginx_proxy
