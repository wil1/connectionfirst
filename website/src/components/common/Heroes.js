import React, { useState, useEffect } from 'react'
import Navigation from './Navigation'
import YouTube from './YouTubeEmbed'

const backgroundImageWidth = 1366
const backgroundImageHeight = 691

export const DefaultHero = ({ site }) => {
    return (
        <>
            <section
                id="default-hero"
                className="section"
                style={{
                    backgroundImage: `url('/images/HomeBannerImage.png')`,
                    backgroundSize: `cover`,
                    backgroundColor: `rgba(255,255,255,0.5)`,
                }}
            >
                <Navigation site={site} />
            </section>
        </>
    )
}

const MissionText = () => (
    <div className="columns is-centered">
        <div className="column has-text-centered">
            <h4 className="title is-4 light-text">
                Our mission is to create spaces where people are seen, heard,
                and valued.
            </h4>
            <span className="subtitle is-6 light-text">
                We use Nonviolent Communication and restorative practices to
                counter the effects of epidemic trauma in all systems:
                government, education, business, industry and homes.
            </span>
        </div>
    </div>
)

const checkIsPortrait = (width, height) => (width < 768) || (height < 500) || (width < height)
export const LandingHero = ({ site }) => {
    const $wnd = typeof window !== `undefined` ? window : {
        innerHeight: backgroundImageHeight,
        innerWidth: backgroundImageWidth,
        addEventListener: () => {},
        removeEventListener: () => {},
        
    }
    
    const [windowWidth, setWindowWidth] = useState($wnd.innerWidth)
    // TODO fix this with proper images and CSS
    const [isPortrait, setIsPortrait] = useState(checkIsPortrait($wnd.innerWidth, $wnd.innerHeight))
    const [clickedPlay, setClickedPlay] = useState(false)
    const handleWindowResize = () => {
        setWindowWidth($wnd.innerWidth)
        setIsPortrait(checkIsPortrait($wnd.innerWidth, $wnd.innerHeight))
    }

    useEffect(() => {
        $wnd.addEventListener(`resize`, handleWindowResize)

        return () => {
            $wnd.removeEventListener(`resize`, handleWindowResize)
        }
    })

    const height =
        (windowWidth / backgroundImageWidth) * backgroundImageHeight - 200

        console.log(`isPortrait`, isPortrait)
    return (
        <>
            <section
                id="landing-hero"
                className="section"
                style={{
                    backgroundImage: `url('/images/HomeBannerImage.png')`,
                    backgroundSize: `cover`,
                    backgroundColor: `rgba(255,255,255,0.5)`,
                }}
            >
                <Navigation site={site} />

                {clickedPlay ? (
                    <YouTube id="lpHzZQFz2rg" appendSrc="?autoplay=1" />
                ) : (
                    <div className="columns is-centered is-vcentered">
                        <div
                            className="column has-text-centered"
                            style={{ height: `${height}px` }}
                        >
                            <span
                                style={{
                                    display: `inline-block`,
                                    height: `100%`,
                                    verticalAlign: `middle`,
                                }}
                            ></span>
                            <a
                                onClick={() => setClickedPlay(true)}
                                className="play-video-link"
                                src="/images/play-video.png"
                                alt="Play video overview"
                            />
                        </div>
                    </div>
                )}
                {isPortrait || <MissionText />}
            </section>
            {isPortrait && <div className="mission-text-portrait"><MissionText /></div>}
        </>
    )
}
