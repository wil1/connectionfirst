import {
    IonButtons,
    IonCard,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonContent,
    IonHeader,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
} from '@ionic/react'
import { book, build, colorFill, grid } from 'ionicons/icons'
import React, { useContext } from 'react'
import './Home.css'
import { AppContext } from '../contexts/AppContext'

const HomePage: React.FC = () => {
    const { user } = useContext(AppContext)
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Home</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonCard className="welcome-card">
                    <IonCardHeader>
                        <IonCardSubtitle>Get Started</IonCardSubtitle>
                        <IonCardTitle>
                            Welcome to Ionic Application
                        </IonCardTitle>
                        {user && (
                            <IonCardSubtitle>
                                Welcome {user.preferredUsername}!
                            </IonCardSubtitle>
                        )}
                    </IonCardHeader>
                </IonCard>

                <IonList lines="none">
                    <IonListHeader>
                        <IonLabel>Resources</IonLabel>
                    </IonListHeader>
                    <IonItem
                        href="https://ionicframework.com/docs/"
                        target="_blank"
                    >
                        <IonIcon slot="start" color="medium" icon={book} />
                        <IonLabel>Ionic Documentation</IonLabel>
                    </IonItem>
                    <IonItem
                        href="https://ionicframework.com/docs/building/scaffolding"
                        target="_blank"
                    >
                        <IonIcon slot="start" color="medium" icon={build} />
                        <IonLabel>Scaffold Out Your App</IonLabel>
                    </IonItem>
                    <IonItem
                        href="https://ionicframework.com/docs/layout/structure"
                        target="_blank"
                    >
                        <IonIcon slot="start" color="medium" icon={grid} />
                        <IonLabel>Change Your App Layout</IonLabel>
                    </IonItem>
                    <IonItem
                        href="https://ionicframework.com/docs/theming/basics"
                        target="_blank"
                    >
                        <IonIcon slot="start" color="medium" icon={colorFill} />
                        <IonLabel>Theme Your App</IonLabel>
                    </IonItem>
                </IonList>
            </IonContent>
        </IonPage>
    )
}

export default HomePage
