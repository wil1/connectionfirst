import * as nodemailer from 'nodemailer'
import * as log from 'consola'

const mailTransport = nodemailer.createTransport(`smtps://smtp-relay.gmail.com`)

export type Message = {
    from: string
    to: string
    subject: string
    text: string
}

export function sendEmail(message: Message) {
    log.info(`Sending email`, message)
    return new Promise((resolve, reject) => {
        mailTransport.sendMail(message, (error, info) => {
            if (error) {
                reject(error)
            } else {
                resolve(info)
            }
        })
    })
}
