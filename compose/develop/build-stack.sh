#!/bin/bash

if [[ ! -e .env ]]; then
    cp .env-dist .env
fi

# loads .env file into the current environment
export $(cat .env | sed 's/#.*//g' | xargs)

docker-compose build
docker-compose push
