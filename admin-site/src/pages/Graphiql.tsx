import {
    IonCard,
    IonPage,
    IonContent,
    IonTitle,
    IonMenuButton,
    IonHeader,
    IonToolbar,
    IonButtons,
} from '@ionic/react'
import useAsyncEffect from '@n1ru4l/use-async-effect'
// const GraphiQL = require('graphiql')
// @ts-ignore
import GraphiQL from 'graphiql'
// @ts-ignore
import GraphiQLExplorer from 'graphiql-explorer'
import 'graphiql/graphiql.css'
import {
    buildClientSchema,
    getIntrospectionQuery,
    GraphQLSchema,
} from 'graphql'
import React, { useState, useContext } from 'react'
import { LoginRequiredPage } from '../components/Auth'
import { AppContext } from '../contexts/AppContext'

import './Graphiql.css'

const graphQLFetcher = (baseDomain: string, token: string) => (
    graphQLParams: any,
    fetchOptions?: any
) => {
    const fetchOpts = fetchOptions || {}
    return fetch(`https://api.${baseDomain}/graphql`, {
        method: 'post',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(graphQLParams),
        ...fetchOpts,
    }).then(response => response.json())
}

type State = {
    schema?: GraphQLSchema
    query: string
    explorerIsOpen: boolean
}

export const GraphiqlPage: React.FC<{ baseDomain: string }> = ({
    baseDomain,
}) => {
    const { token } = useContext(AppContext)
    const [state, setState] = useState<State>({
        query: ``,
        explorerIsOpen: true,
    })

    const fetcher = graphQLFetcher(baseDomain, token || ``)
    useAsyncEffect(function*(onCancel) {
        if (!token) {
            return
        }
        const result = yield fetcher({
            query: getIntrospectionQuery(),
        })
        setState({ ...state, schema: buildClientSchema(result.data) })
    }, [])

    if (!token) {
        return <LoginRequiredPage />
    }

    if (!state.schema) {
        return (
            <IonPage>
                <IonCard>Loading schema...</IonCard>
            </IonPage>
        )
    }

    const onEditQuery = (query: string) => {
        console.log(`onEdit`, query)
        setState({ ...state, query: query })
    }

    const toggleExplorer = () =>
        setState({ ...state, explorerIsOpen: !state.explorerIsOpen })

    const explorer = (
        <GraphiQLExplorer
            schema={state.schema}
            query={state.query}
            onEdit={onEditQuery}
            // onRunOperation={operationName =>
            //   this._graphiql.handleRunQuery(operationName)
            // }
            explorerIsOpen={state.explorerIsOpen}
            onToggleExplorer={toggleExplorer}
            // getDefaultScalarArgValue={getDefaultScalarArgValue}
            // makeDefaultArg={makeDefaultArg}
        />
    )

    const editor = (
        <GraphiQL
            fetcher={fetcher}
            onEditQuery={onEditQuery}
            query={state.query}
        >
            <GraphiQL.Toolbar>
                <GraphiQL.Button
                    onClick={toggleExplorer}
                    label="Explorer"
                    title="Toggle Explorer"
                />
            </GraphiQL.Toolbar>
        </GraphiQL>
    )

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>GraphiQL</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                <div className="graphiql-container">
                    {explorer}
                    {editor}
                </div>
            </IonContent>
        </IonPage>
    )
}
