import {
    IonContent,
    IonHeader,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonMenu,
    IonMenuToggle,
    IonTitle,
    IonToolbar,
} from '@ionic/react'
import React, { useContext } from 'react'
import { RouteComponentProps, withRouter } from 'react-router-dom'
import { AppPage } from '../declarations'
import { logOut, logIn } from 'ionicons/icons'
import { AppContext } from '../contexts/AppContext'

interface MenuProps extends RouteComponentProps {
    appPages: AppPage[]
}

const Menu: React.FunctionComponent<MenuProps> = ({ appPages }) => {
    const {user} = useContext(AppContext)

    return (
        <IonMenu contentId="main" type="overlay">
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Menu</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonList>
                    {appPages.map((appPage, index) => {
                        return (
                            <IonMenuToggle key={index} autoHide={false}>
                                <IonItem
                                    routerLink={appPage.url}
                                    routerDirection="none"
                                >
                                    <IonIcon slot="start" icon={appPage.icon} />
                                    <IonLabel>{appPage.title}</IonLabel>
                                </IonItem>
                            </IonMenuToggle>
                        )
                    })}
                    {user ? (
                        <IonMenuToggle key="logout" autoHide={false}>
                            <IonItem
                                routerLink="/logout"
                                routerDirection="none"
                            >
                                <IonIcon slot="start" icon={logOut} />
                                <IonLabel>Log out</IonLabel>
                            </IonItem>
                        </IonMenuToggle>
                    ) : (
                        <IonMenuToggle key="login" autoHide={false}>
                            <IonItem routerLink="/login" routerDirection="none">
                                <IonIcon slot="start" icon={logIn} />
                                <IonLabel>Log in</IonLabel>
                            </IonItem>
                        </IonMenuToggle>
                    )}
                </IonList>
            </IonContent>
        </IonMenu>
    )
}
export default withRouter(Menu)
