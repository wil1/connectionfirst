import React from 'react'
import {
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonRouterLink,
    IonPage,
} from '@ionic/react'

export const LoginRequiredPage: React.FC = () => (
    <IonPage>
        <IonCard>
            <IonCardHeader>
                <IonCardTitle>Login required</IonCardTitle>
            </IonCardHeader>
            <IonCardContent>
                <IonRouterLink routerLink="/login">Log in</IonRouterLink>
            </IonCardContent>
        </IonCard>
    </IonPage>
)
