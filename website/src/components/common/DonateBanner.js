import React from 'react'
import { Link } from 'gatsby'

const BoxLetters = ({ text }) => Array.prototype.map.call(text, (char, index) => {
    const isDigit = char >= `0` && char <= `9`

    return (
        <span className={isDigit ? `donation-box-digit` : `donation-box`} key={index}>
            {char}
        </span>
    )
})

export const DonateBanner = () => (
    <section className="section donate-banner">
        <div className="columns is-centered is-vcentered">
            <div className="column has-text-centered is-one-quarter">
                <h3 className="title is-3 light-text">
                    <BoxLetters text="$30,973" />
                </h3>
            </div>
            <div className="column has-text-centered is-half">
                <h3 className="title is-3 light-text">
                    of our $86,995 goal raised
                </h3>
            </div>
            <div className="column has-text-centered is-one-quarter">
                <a href="https://ioby.org/project/nonviolent-communication-and-restorative-justice-leon-county-schools" className="button donate-banner-button is-large">
                    DONATE
                </a>
            </div>
        </div>
    </section>
)
