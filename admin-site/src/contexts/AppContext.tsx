import React from "react"
export type User = {
    id: number,
    externalId: string,
    preferredUsername: string
}
export type AppContextProps = {
    user?: User
    token?: string
}
export const AppContext = React.createContext({} as AppContextProps)
