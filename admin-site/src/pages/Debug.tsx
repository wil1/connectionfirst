import {
    IonButtons,
    IonCard,
    IonCardContent,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonItem,
    IonItemDivider,
    IonLabel,
    IonList,
    IonLoading,
    IonMenuButton,
    IonPage,
    IonRow,
    IonText,
    IonTextarea,
    IonTitle,
    IonToolbar,
} from '@ionic/react'
import useAsyncEffect from '@n1ru4l/use-async-effect'
import React, { useContext, useState } from 'react'
import ReactJson from 'react-json-view'
import { useQuery } from 'urql'
import { AppContext } from '../contexts/AppContext'

const getMigrations = `
query getMigrations {
  migrations {
    nodes {
      id
      name
      executedAt
      hash
    }
  }
}
`

const MigrationList: React.FC = () => {
    const [res] = useQuery({
        query: getMigrations,
        variables: {},
    })
    if (res.fetching) {
        return <IonLoading isOpen={true} />
    } else if (res.error) {
        return <IonText color="danger">{JSON.stringify(res.error)}</IonText>
    }

    return (
        <IonGrid>
            {res.data.migrations.nodes.map((migration: any) => (
                <IonRow key={migration.id}>
                    {/* {//TODO - set up proper size breaks for other resolutions} */}
                    <IonCol size="1">{migration.id}</IonCol>
                    <IonCol size="4">{migration.name}</IonCol>
                    <IonCol size="3">{migration.executedAt}</IonCol>
                    <IonCol size="4">{migration.hash}</IonCol>
                </IonRow>
            ))}
        </IonGrid>
    )
}

export type DebugPageProps = {
    baseDomain: string
}

const decodeToken = async (token: string, baseDomain: string) => {
    const res = await fetch(`https://api.${baseDomain}/decode-token`, {
        method: 'get',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
    })
    return await res.json()
}

const TokenViewer: React.FC<DebugPageProps & { token: string }> = ({
    baseDomain,
    token,
}) => {
    const [state, setState] = useState<any>({})
    useAsyncEffect(function*(onCancel) {
        try {
            const result = yield decodeToken(token, baseDomain)
            setState({ ...state, decodedToken: result })
        } catch (e) {
            setState({ ...state, decodedToken: JSON.parse(JSON.stringify(e)) })
        }
    }, [])

    return state.decodedToken ? (
        <ReactJson src={state.decodedToken} />
    ) : (
        <IonText>No token decoded</IonText>
    )
}

export const DebugPage: React.FC<DebugPageProps> = ({ baseDomain }) => {
    const { user, token } = useContext(AppContext)

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Debug</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                <IonCard>
                    <IonCardContent>
                        <IonList>
                            <IonItemDivider>Auth</IonItemDivider>
                            <IonItem>
                                <IonLabel>User</IonLabel>
                                <IonText slot="start">
                                    {user ? user.preferredUsername : ``}
                                </IonText>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Token</IonLabel>
                                <IonTextarea
                                    rows={15}
                                    value={token || ``}
                                    readonly
                                ></IonTextarea>
                            </IonItem>
                            {token && (
                                <IonItem>
                                    <IonItem slot="start">
                                        <TokenViewer
                                            baseDomain={baseDomain}
                                            token={token}
                                        />
                                    </IonItem>
                                </IonItem>
                            )}
                            <IonItemDivider>Migrations</IonItemDivider>
                            <IonItem>
                                <MigrationList />
                            </IonItem>
                        </IonList>
                    </IonCardContent>
                </IonCard>
            </IonContent>
        </IonPage>
    )
}
