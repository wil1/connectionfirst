import { Navbar } from 'bloomer/lib/components/Navbar/Navbar'
import { NavbarBrand } from 'bloomer/lib/components/Navbar/NavbarBrand'
import { NavbarBurger } from 'bloomer/lib/components/Navbar/NavbarBurger'
import { NavbarEnd } from 'bloomer/lib/components/Navbar/NavbarEnd'
import { NavbarItem } from 'bloomer/lib/components/Navbar/NavbarItem'
import { NavbarMenu } from 'bloomer/lib/components/Navbar/NavbarMenu'
import React, { useState } from 'react'
import { NavbarStart } from 'bloomer/lib/components/Navbar/NavbarStart'

const NavItem = ({ href, children }) => (
    <NavbarItem className="bold-nav-item" href={href}>
        {children}
    </NavbarItem>
)

const Navigation = ({ site }) => {
    const [menuVisible, setMenuVisible] = useState(false)
    return (
        <Navbar>
            <NavbarBrand>
                <NavbarStart>
                    <NavItem href="/" key="logo">
                        Connection<br/>First<br/>Inc.
                    </NavItem>
                </NavbarStart>
                <NavbarEnd>
                    <NavItem key="burger">
                        <NavbarBurger
                            isActive={false}
                            onClick={() => setMenuVisible(visible => !visible)}
                        />
                    </NavItem>
                </NavbarEnd>
            </NavbarBrand>
            <NavbarMenu isActive={menuVisible}>
                <NavbarEnd>
                    {site.navigation.map((navItem, i) => {
                        if (navItem.url.match(/^\s?http(s?)/gi)) {
                            return (
                                <a
                                    href={navItem.url}
                                    key={i}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    {navItem.label}
                                </a>
                            )
                        } else {
                            return (
                                <NavItem href={navItem.url} key={i}>
                                    {navItem.label}
                                </NavItem>
                            )
                        }
                    })}
                    <NavItem href="/contact" key="contact">
                        Contact Us
                    </NavItem>
                    <NavItem key="donate">
                        <a href="https://ioby.org/project/nonviolent-communication-and-restorative-justice-leon-county-schools" className="button is-warning navbar-donate-button">
                            DONATE
                        </a>
                    </NavItem>
                </NavbarEnd>
            </NavbarMenu>
        </Navbar>
    )
}
export default Navigation
