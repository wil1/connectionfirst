import { Router } from '@reach/router'
import { graphql } from 'gatsby'
import React from 'react'
import { Layout, PostCard } from '../components/common'
import { DonateBanner } from '../components/common/DonateBanner'
import { MetaData } from '../components/common/meta'

const frontPagePrefix = `frontpage-`

const findTagByPrefix = (tags, prefix) => {
    let result = ``
    if (tags && tags.length) {
        for (let i = 0; i < tags.length && result === ``; i++) {
            const name = tags[i].name.toLowerCase()
            if (name.startsWith(prefix)) {
                result = name
            }
        }
    }
    return result
}

const isFrontPagePost = post => findTagByPrefix(post.node.tags, frontPagePrefix) !== ``

const comparePostsByTagPrefix = (a, b) => {
    const aTag = findTagByPrefix(a.node.tags, frontPagePrefix)
    const bTag = findTagByPrefix(b.node.tags, frontPagePrefix)

    if (aTag > bTag) {
        return 1
    }
    if (aTag < bTag) {
        return -1
    }
    return 0
}

const DefaultPage = ({ posts, pageContext }) => {
    const frontPagePosts = posts.filter(isFrontPagePost)
    frontPagePosts.sort(comparePostsByTagPrefix)
    return (
        <>
            <DonateBanner />

            <div className="columns is-multiline is-gapless">
                {frontPagePosts.map(({ node }) => (
                    <div
                        className="column is-full-mobile is-full-tablet is-half-desktop is-half-widescreen is-one-third-fullhd"
                        key={node.id}
                    >
                        <PostCard post={node} />
                    </div>
                ))}
            </div>
        </>
    )
}

/**
 * Main index page (home page)
 *
 * Loads all posts from Ghost and uses pagination to navigate through them.
 * The number of posts that should appear per page can be setup
 * in /utils/siteConfig.js under `postsPerPage`.
 *
 */
const Index = ({ data, location, pageContext }) => {
    const posts = data.allGhostPost.edges

    return (
        <>
            <MetaData location={location} />
            <Layout isHome={true}>
                <Router>
                    <DefaultPage
                        posts={posts}
                        pageContext={pageContext}
                        path="/"
                    />
                </Router>
            </Layout>
        </>
    )
}

export default Index

// This page query loads all posts sorted descending by published date
// The `limit` and `skip` values are used for pagination
export const pageQuery = graphql`
    query GhostPostQuery($limit: Int!, $skip: Int!) {
        allGhostPost(
            sort: { order: DESC, fields: [published_at] }
            limit: $limit
            skip: $skip
        ) {
            edges {
                node {
                    ...GhostPostFields
                }
            }
        }
    }
`
