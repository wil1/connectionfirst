import React from 'react'
import { Link } from 'gatsby'

const postHeight = 800

const RightArrow = () => (
    <i className="fas fa-chevron-right" style={{ marginLeft: `.5em` }}></i>
)

const PostCard = ({ post }) => {
    const url = `/${post.slug}/`

    if (post.feature_image) {
        return (
            <Link
                to={url}
                style={{
                    display: `inline-block`,
                    backgroundImage: `url(${post.feature_image})`,
                    backgroundSize: `cover`,
                    height: `${postHeight}px`,
                    width: `100%`,
                    padding: `2em`,
                }}
            >
                <h3 className="title is-3 light-text has-text-right">
                    {post.title}
                    <RightArrow />
                </h3>
                <div
                    className="subtitle is-5 light-text has-text-left"
                    style={{ marginTop: `2em` }}
                >
                    {post.excerpt}
                </div>
            </Link>
        )
    }

    return (
        <div
            style={{
                display: `inline-block`,
                height: `${postHeight}px`,
                width: `100%`,
                padding: `2em`,
            }}
        >
            <Link to={url}>
                <h2 className="title is-2 has-text-centered">{post.title}</h2>
            </Link>
            <div className="is-divider" />
            <span>{post.excerpt}</span>

            <div className="has-text-centered">
                <Link to={url} className="button is-large">Learn more <RightArrow /></Link>
            </div>
            
        </div>
    )
}

export default PostCard
