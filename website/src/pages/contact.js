import React, { useState } from 'react'
import { Layout } from '../components/common'

function newContact() {
    return {
        name: ``,
        email: ``,
        subject: ``,
        message: ``,
        status: `draft`,
    }
}

async function sendContact(contact) {
    const { name, email, subject, message } = contact
    // Default options are marked with *
    const response = await fetch(`https://api.${window ? window.location.host : ``}/contact/send`, {
        method: `POST`, // *GET, POST, PUT, DELETE, etc.
        mode: `cors`, // no-cors, *cors, same-origin
        cache: `no-cache`, // *default, no-cache, reload, force-cache, only-if-cached
        credentials: `same-origin`, // include, *same-origin, omit
        headers: {
            'Content-Type': `application/json`,
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: `follow`, // manual, *follow, error
        referrerPolicy: `no-referrer`, // no-referrer, *client
        body: JSON.stringify({ name, email, subject, message }), // body data type must match "Content-Type" header
    })
    return await response.json() // parses JSON response into native JavaScript objects
}

const blankContact = newContact()

function completed(input) {
    return input && input.trim().length > 0
}

const ContactPage = () => {
    const [state, setState] = useState(blankContact)
    const { status } = state

    function makeInputProps(fieldName) {
        const fieldValue = state[fieldName]
        const handler = e =>
            setState({ ...state, [fieldName]: e.target.value || `` })
        return {
            name: fieldName,
            value: fieldValue,
            onChange: handler,
            disabled: state.status !== `draft`,
        }
    }

    const name = makeInputProps(`name`)
    const email = makeInputProps(`email`)
    const subject = makeInputProps(`subject`)
    const message = makeInputProps(`message`)

    function isValid() {
        return (
            completed(name.value) &&
            completed(email.value) &&
            completed(subject.value) &&
            completed(message.value)
        )
    }

    return (
        <Layout>
            <div className="container page-container">
                <div className="columns is-multiline">
                    <div className="column is-full">
                        <h1 className="title is-1">Contact Us</h1>
                    </div>
                    <div className="column is-full">
                        <div className="field">
                            <label className="label">Name</label>
                            <div className="control">
                                <input
                                    type="text"
                                    className="input"
                                    {...name}
                                />
                            </div>
                        </div>

                        <div className="field">
                            <label className="label">Email Address</label>
                            <div className="control">
                                <input
                                    type="text"
                                    className="input"
                                    {...email}
                                />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Subject</label>
                            <div className="control">
                                <input
                                    type="text"
                                    className="input"
                                    {...subject}
                                />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label"></label>
                            <div className="control">
                                <textarea className="textarea" {...message} />
                            </div>
                        </div>
                    </div>
                    <div className="column is-full">
                        {(function() {
                            switch (status) {
                                case `sent`:
                                    return (
                                        <button
                                            className="button is-large is-success"
                                            disabled
                                        >
                                            Message sent
                                            <i className="fas fa-check inline-margin-icon"></i>
                                        </button>
                                    )

                                case `sending`:
                                    return (
                                        <button
                                            className="button is-large"
                                            disabled
                                        >
                                            Sending message
                                            <i className="fas fa-ellipsis-h inline-margin-icon"></i>
                                        </button>
                                    )
                                default:
                                    return (
                                        <button
                                            className="button is-large"
                                            onClick={() => {
                                                console.log(
                                                    `Sending message`,
                                                    state
                                                )
                                                setState({
                                                    ...state,
                                                    status: `sending`,
                                                })
                                                sendContact(state).then(
                                                    response => {
                                                        console.log(
                                                            `sendContact result`,
                                                            response
                                                        )

                                                        setState({
                                                            ...state,
                                                            status: `sent`,
                                                        })
                                                    }
                                                )
                                            }}
                                            disabled={
                                                status !== `draft` || !isValid()
                                            }
                                        >
                                            Send Message
                                            <i
                                                className="fas fa-envelope inline-margin-icon"
                                            ></i>
                                        </button>
                                    )
                            }
                        })()}
                    </div>
                </div>
            </div>
        </Layout>
    )
}
export default ContactPage
